import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIURL, cabecera } from '../shared/APIURL';


@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private _http: HttpClient){ }


// eslint-disable-next-line @typescript-eslint/member-ordering
filtroCovid = {
  code: '',
  ciudad: '',
  fecha:'',

};

public getCiudadXname(){
   const param = 'name=' + this.filtroCovid.ciudad;
  return this._http.get<any>(APIURL.covid.dataUltimaCiudadXName + param,{ headers: cabecera }  );
}

public getCiudadXnameXDate(){
  const param = 'name=' + this.filtroCovid.ciudad +
   '&date='+ this.filtroCovid.fecha;

 return this._http.get<any>(APIURL.covid.dataDiariaXName + param, { headers: cabecera } );
}
public getCiudadXCode(code: string){
 return this._http.get<any[]>(APIURL.covid.dataUltima_CiudadXCode + code , {headers: cabecera } );
}



}
