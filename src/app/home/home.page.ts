import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { HomeService } from './home.service';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  formCovid: FormGroup;
    ciudadCode: any[];
    mostrarCode = false;
    mostrarCiudad = false;
    mostrarFecha = false;
    mostrarText = '';
    // eslint-disable-next-line @typescript-eslint/naming-convention
    ReporteNumero: number;

  constructor(private _homeSer: HomeService,
              private _formB: FormBuilder,
              private _toast: ToastController ) {}


  ngOnInit(): void {
     // this.getDataCiudaCodigo();
      this.Formulario();
  }

    public Formulario(){
      this.formCovid = this._formB.group({
        code: ['',Validators.required],
        ciudad: ['', Validators.required],
        fecha: ['', Validators.required]
      });
    }

public SeleccionReported(i: number){
    this.ReporteNumero = Number(i);
  switch(this.ReporteNumero){
     case 1:
       this.mostrarCode = true;
       this.mostrarText = "Codigo";
       this.mostrarFecha= false;
        this.mostrarCiudad = false;
      break;

      case 2:
        this.mostrarCode = false;
        this.mostrarFecha= false;
         this.mostrarCiudad = true;
         this.mostrarText = "Ciudad";
      break;

      case 3:
        this.mostrarCode = false;
        this.mostrarFecha= true;
         this.mostrarCiudad = true;
         this.mostrarText = "Ciudad";
        break;

      case 4:
        this.mostrarCode = false;
        this.mostrarFecha= true;
         this.mostrarCiudad = false;
         this.mostrarText = 'Fecha';
       break;

   }
}

    public BuscarCovid(){

      // eslint-disable-next-line no-debugger
      // eslint-disable-next-line no-underscore-dangle
          this._homeSer.filtroCovid.code = this.formCovid.controls.code.value;
          // eslint-disable-next-line no-underscore-dangle
          this._homeSer.filtroCovid.ciudad = this.formCovid.controls.ciudad.value;
          // eslint-disable-next-line no-underscore-dangle
          this._homeSer.filtroCovid.fecha = this.formCovid.controls.fecha.value;

         this.getOpciones();
    };


  public getDataCiudaCodigo(){
        // eslint-disable-next-line no-underscore-dangle
        // eslint-disable-next-line no-debugger
        // eslint-disable-next-line no-underscore-dangle
        this._homeSer.getCiudadXCode(this._homeSer.filtroCovid.code).subscribe(async res =>{

          if ( res.length > 0 ) {
            this.ciudadCode = res;
          } else {

            // eslint-disable-next-line no-underscore-dangle
            const toast = await this._toast.create({
              message: 'Error al cargar el reporte',
              duration: 2000,
              position: 'bottom'
            });

            toast.present();
          }
        });
  }

  public getDataXCiudad(){

        // eslint-disable-next-line no-underscore-dangle
        this._homeSer.getCiudadXname().subscribe( async res => {
        if (res.length > 0){
          this.ciudadCode = res;
        } else {
          // eslint-disable-next-line no-underscore-dangle
          const toast = await this._toast.create({
            message: 'Error al cargar el reporte',
            duration: 2000,
            position: 'top'
          });

          toast.present();
        }
   });
  }

  public getDataCiudadXName(){
    // eslint-disable-next-line no-underscore-dangle
    this._homeSer.getCiudadXnameXDate().subscribe( async res => {
      if (res.length > 0){
        this.ciudadCode = res;
      } else {
        // eslint-disable-next-line no-underscore-dangle
        const toast = await this._toast.create({
          message: 'Error al cargar el reporte',
          duration: 2000,
          position: 'top'
        });

        toast.present();
      }
 });
  }



public async getOpciones(){
    const i = this.ReporteNumero;
   switch( i ){

    case 1:
      this.getDataCiudaCodigo();
      break;

      case 2:
        this.getDataXCiudad();
      break;

      case 3:
        this.getDataCiudadXName();
        break;


        case 4:
          break;

    default:
    // eslint-disable-next-line no-underscore-dangle
    const toast = await this._toast.create({
      message: 'Error al cargar el reporte',
      duration: 2000,
      position: 'top'
    });

    toast.present();

  }
}







}

