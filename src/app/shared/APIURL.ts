import { environment} from '../../environments/environment';


const BASEURL = environment.URL;
 export const cabecera = environment.headers;


export const APIURL = {

    covid: {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            dataUltima_CiudadXCode: BASEURL + 'country/code?code=',
            dataDiariaXName: BASEURL + 'report/country/name?',
            dataDiariaXCode: BASEURL + 'report/country/code?',
            dataUltimaCiudadXName: BASEURL + 'country?'
    }


};
